# Low-Latency Non-Line-of-Sight Imaging

See https://biostat.wisc.edu/~compoptics/rt_nlos21/rt_nlos.html for detailed information on this project.

Sample data files having the extension *.out can be downloaded from https://biostat.wisc.edu/~compoptics/rt_nlos21/data/

### Building From Source

This source can be built with CMake and has been tested with the following configuration:

* Windows 10
* Visual Studio 2019
* CUDA Toolkit 10.2
* OpenCV 4.2.0

Running with the ```-h``` command line option will provide a list of command line options.

### Typical usage

```c:\nlos>realtime_nlos.exe -s sample_parameters.yml -f raw_data_file.out ```

### Command Line Options


```
c:\nlos>realtime_nlos.exe -s sample_parameters.yml -h

Usage:
  realtime_nlos.exe [OPTION...]

 FastRSDImageReconstructor options:
      --no-dda  do NOT use depth dependent averaging
 General options:
  -h, --help       Show help
  -d, --debug      enable debug output
  -s, --scene arg  Scene parameters file (yml)

 ImageDisplay options:
      --no-adaptive-threshold  Fix the scene threshold limits based on first
                               frame
      --bandpass-low arg       lower threshold for 2D bandpass filter [0-1]
      --bandpass-high arg      upper threshold for 2D bandpass filter [0-1]

 LogWriter options:
  -l, --logdir arg  log file directory
      --lograw      log raw T3 records
      --logpar arg  log type for parsed records (bin|yml)
      --logfdh arg  log type for FDH records (bin|yml)
      --logrsd arg  log type for RSD records (bin|yml)
      --logimg arg  log type for output images (png|bin|yml|monopng)

 RawSensorDataReader options:
  -f, --file arg  read raw data from file

Keyboard Shortcuts while running:
  Q, q    Quit
  H, h    Print help
  V, v    Toggle verbose output
  C, c    Cycle colormap forward / backward
  Z, z    Decrease / Increase window size (zoom out / in)
  R, r    Reset window size to 1:1
  T       Enable adaptive scene threshold
  t       Disable adaptive scene threshold
  B       Reset bandpass filter to [0.1, 0.9]
  b       Set bandpass filter to [0.0, 1.0] (disabled)
  {       Decrease bandpass f_high by 10%
  }       Increase bandpass f_high by 10%
  [       Decrease bandpass f_low by 10%
  ]       Increase bandpass f_low by 10%
  D       Turn ON depth dependent averaging
  d       Turn OFF depth dependent averaging
```