#pragma once

#include "../types.h"
#include "../util/DataProcessor.h"
#include "opencv2/core/core.hpp"
#include "opencv2/imgproc/imgproc.hpp"
#include "opencv2/highgui/highgui.hpp"
#include "../util/LogWriter.h"

namespace NLOS {

    // stage 4: Display the final 2D images to a window as they become available.
    class ImageDisplay : public DataProcessor {
    public:
        ImageDisplay(ReconstructedImageDataQueue& incoming, KeyboardInputQueue& keyboardInput, LogWriter& logWriter)
            : DataProcessor("ImageDisplay", logWriter)
            , m_incomingImages(incoming)
            , m_keyboardInput(keyboardInput)
            , m_colorMap(cv::COLORMAP_HOT)
            , m_scaleFactor(1.0)
            , m_useAdaptiveThreshold(false)
            , m_optDisableAdaptiveThreshold(false)
            , m_bandpassFilter({ 0.1, 0.9 })
            , m_optBandpassLow(0.1)
            , m_optBandpassHigh(0.9)
        {}

        void CycleColorMap(int incr = 1);
        void SetColorMap(int colorMap = cv::COLORMAP_HOT);

        // zoom factor
        void AdjustScale(double factor = 1.0);
        void SetScale(double factor = 1.0);

        // thresholding
        void EnableAdaptiveThreshold(bool enable);
        void AdjustBandpassFilter(double factorLow, double factorHigh);
        void SetBandpassFilter(double low, double high);

    protected:
        virtual void InitCmdLineOptions(cxxopts::Options& options);
        virtual void Initialize(const SceneParameters& sceneParameters);
        virtual void Work();
        virtual void OnStop();
    private:
        ReconstructedImageDataQueue& m_incomingImages;
        KeyboardInputQueue& m_keyboardInput;
        std::atomic<int> m_colorMap;
        std::atomic<double> m_scaleFactor;
        
        std::atomic<bool> m_useAdaptiveThreshold;
        std::pair<double, double> m_bandpassFilter;

        bool m_optDisableAdaptiveThreshold;
        double m_optBandpassLow;
        double m_optBandpassHigh;

        const std::string m_window_name = "NLOS Image Streaming";
    };
}
