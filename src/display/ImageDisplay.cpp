#include <algorithm>
#include "../util/spdlog/spdlog.h"
#include "ImageDisplay.h"
#include "../util/stopwatch.h"


namespace NLOS {

    void ImageDisplay::InitCmdLineOptions(cxxopts::Options& options)
    {
        options.add_options(m_name)
            ("no-adaptive-threshold", "Fix the scene threshold limits based on first frame", cxxopts::value<bool>(m_optDisableAdaptiveThreshold))
            ("bandpass-low", "lower threshold for 2D bandpass filter [0-1]", cxxopts::value<double>(m_optBandpassLow))
            ("bandpass-high", "upper threshold for 2D bandpass filter [0-1]", cxxopts::value<double>(m_optBandpassHigh));
    }


    void ImageDisplay::Initialize(const SceneParameters& sceneParameters)
    {
        EnableAdaptiveThreshold(!m_optDisableAdaptiveThreshold);
        double low = std::max(m_optBandpassLow, 0.0);
        double high = std::min(m_optBandpassHigh, 1.0);
        if (high < low)
            std::swap(high, low);
        SetBandpassFilter(low, high);
    }

    // This worker should receive 2D images from the incoming ReconstructedImageDataQueue
    // It should display the images to an output window
    void ImageDisplay::Work() {
        try {
            ReconstructedImageDataPtr imageData;
            Stopwatch timer(true);

            cv::Mat tmp_scaled(200, 200, CV_32FC1);
            tmp_scaled = 0.f;

            std::vector<double> mins;
            std::vector<double> maxs;
            const int window_size = 10;

            while (!m_stopRequested) {
                // get the next frame of histogram data
                bool gotOne = m_incomingImages.Pop(imageData);
                if (m_stopRequested) {
                    break;
                }

                if (gotOne) {
                    double minVal;
                    double maxVal;
                    cv::Point minLoc;
                    cv::Point maxLoc;

                    // find the minimal and maximam value with their locations
                    minMaxLoc(imageData->Image2d, &minVal, &maxVal, &minLoc, &maxLoc);

                    if (m_useAdaptiveThreshold) { // fixed threshold values for every frame.
                        if (mins.size() < window_size) {
                            mins.push_back(minVal);
                            maxs.push_back(maxVal);
                        }
                        else
                        {
                            mins[imageData->FrameNumber % window_size] = minVal;
                            maxs[imageData->FrameNumber % window_size] = maxVal;
                        }

                        minVal = *std::min_element(mins.begin(), mins.end());
                        maxVal = *std::max_element(maxs.begin(), maxs.end());
                    }
                    spdlog::debug(",{:<25},{}, Image min:{:.2f}, max:{:.2f}", m_name, imageData->FrameNumber, minVal, maxVal);
                    cv::Mat tmp_mgn = (imageData->Image2d - minVal) / (maxVal - minVal);

                    cv::Mat matRotation = cv::getRotationMatrix2D(cv::Point(tmp_mgn.rows / 2, tmp_mgn.cols / 2), 90, 1);
                    cv::Mat imgRotated;
                    cv::warpAffine(tmp_mgn, imgRotated, matRotation, tmp_mgn.size());
                    tmp_mgn = imgRotated;

                    // visualization
                    tmp_mgn.convertTo(tmp_mgn, CV_8UC1, 255.0, 0); // automatically clamps values that are < 0 || > 1 via a saturation_cast<>
                    
                    // apply bandpass (for 190x190 takes ~0.1ms on i7-8700k...no need to optimize right now)
                    int npxls = tmp_mgn.rows * tmp_mgn.cols;
                    auto bf = m_bandpassFilter;
                    if (bf.first > 0.0 || bf.second < 1.0) {
                        uchar lf = (uchar)(bf.first * 255.);
                        uchar hf = (uchar)(bf.second * 255.);
                        if (lf < hf) {
                            float scale = 255.0f / (hf - lf); 
                            
                            uchar * p = tmp_mgn.data; // addr first pixel
                            uchar* pend = p + tmp_mgn.rows * tmp_mgn.cols; // addr past last pixel
                            while (p < pend) { // loop through all pixels
                                uchar vs = std::max(*p - lf, 0); // shift by lower limit
                                uchar vd = (uchar)std::min(int(vs * scale + 0.5f), 255); // scale
                                *p = vd; // store
                                p++;     // advance
                            }
                        }
                    }

                    // if we are to log 8-bit mono png image, we need to do it before applying color map.
                    if (m_logWriter.LogImageData() && m_logWriter.ImageLogFormat() == LogFileFormat::MonoPng) {
                        ReconstructedImageDataPtr img(new ReconstructedImageDataType(imageData->FrameNumber, tmp_mgn));
                        m_logWriter.PushLog(img);
                    }

                    //cv::applyColorMap(tmp_mgn, tmp_mgn, m_colorMap);

                    // if we are to log png, binary, yml, we need to do it after applying color map.
                    if (m_logWriter.LogImageData() && m_logWriter.ImageLogFormat() != LogFileFormat::MonoPng) {
                        ReconstructedImageDataPtr img(new ReconstructedImageDataType(imageData->FrameNumber, tmp_mgn));
                        m_logWriter.PushLog(img);
                    }

                    cv::resize(tmp_mgn, tmp_scaled, cv::Size(), m_scaleFactor, m_scaleFactor);
                    cv::namedWindow(m_window_name, cv::WINDOW_AUTOSIZE);

                    auto time = timer.Stop(); // this will measure the time between receiving frames.
                    timer.Start(); // start timing for the next frame
                    double avg = timer.Average();
                    double fps = time != 0. ? 1000. / time : 0.0;
                    double fpsavg = avg != 0. ? 1000. / avg : 0.0;

                    spdlog::debug(",{:<25},{},{:8.2f} ms to receive image & display frame. (fps:{:.2f}, avg:{:.2f} ms, avgfps:{:.2f}", m_name, imageData->FrameNumber, time, fps, avg, fpsavg);
                }

                if (tmp_scaled.rows > 0 && tmp_scaled.cols > 0) {
                    cv::imshow(m_window_name, tmp_scaled);
                    int ch = cv::waitKey(20);
                    // see if window is still open
                    if (cv::getWindowProperty(m_window_name, cv::WND_PROP_VISIBLE) <= 0) {
                        spdlog::critical("{:<25}: User closed image window. Shutting down.", m_name);
                        ch = 'q'; // send the 'q' key to trigger a shutdown
                    }

                    if (ch >= 0) {
                        spdlog::trace("{:<25}: Received keyboard character {}.", m_name, (char)ch);
                        m_keyboardInput.Push((char)ch);
                    }
                }
                else {
                    std::this_thread::sleep_for(std::chrono::milliseconds(50));
                }

                if (m_stopRequested) {
                    break;
                }
            }
            cv::destroyWindow(m_window_name);
        }
        catch (const std::exception & ex) {
            spdlog::critical("{}: Exception: {}", m_name, ex.what());
            m_keyboardInput.Push('q'); // exit the app
        }

        spdlog::trace("{:<25}: Exiting worker thread", m_name);

    }
	void ImageDisplay::OnStop() { 
        // there may be a push blocking if the queue is full, so tell the queue to abort it's operation
        spdlog::trace("{:<25}: Abort", m_name);
        m_incomingImages.Abort();
    }

    void ImageDisplay::CycleColorMap(int incr) {
        const int NUM_COLOR_MAPS = cv::COLORMAP_TWILIGHT_SHIFTED + 1; // the last colormap listed in imageproc.hpp
        
        int colorMapCur = m_colorMap;
        colorMapCur = (colorMapCur + incr) % NUM_COLOR_MAPS;
        m_colorMap = colorMapCur;
    }
    void ImageDisplay::SetColorMap(int colorMap) {
        if (colorMap < cv::COLORMAP_AUTUMN || colorMap > cv::COLORMAP_TWILIGHT_SHIFTED)
            colorMap = cv::COLORMAP_HOT;
        m_colorMap = colorMap;
    }

    void ImageDisplay::AdjustScale(double factor) {
        double newScale = m_scaleFactor * factor;
        newScale = std::min(5.0, std::max(1.0, newScale));
        m_scaleFactor = newScale;
    }
    void ImageDisplay::SetScale(double scale)
    {
        m_scaleFactor = std::min(5.0, std::max(1.0, scale));
    }

    void ImageDisplay::EnableAdaptiveThreshold(bool enable) {
        m_useAdaptiveThreshold = enable;
        if (m_useAdaptiveThreshold)
            spdlog::info("Adaptive scene threshold ENABLED");
        else
            spdlog::info("Adaptive scene threshold DISABLED");
    }
    void ImageDisplay::AdjustBandpassFilter(double factorLow, double factorHigh) {
        auto bp = m_bandpassFilter;
        bp.first = std::max(bp.first * factorLow, 0.0);
        bp.second = std::min(bp.second * factorHigh, 1.0);
        if (bp.first > bp.second) {
            std::swap(bp.first, bp.second);
        }
        m_bandpassFilter = bp;
        spdlog::info("Bandpass filter is now [{:.2f}, {:.2f}]", bp.first, bp.second);
    }
    void ImageDisplay::SetBandpassFilter(double low, double high) {
        m_bandpassFilter = std::pair<double, double>({ low, high });
        spdlog::info("Bandpass filter is now [{:.2f}, {:.2f}]", m_bandpassFilter.first, m_bandpassFilter.second);
    }
}
