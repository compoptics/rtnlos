#pragma once

#include "../types.h"
#include "../util/DataProcessor.h"
#include "../util/LogWriter.h"

#define USE_FDH_LOOKUP_TABLE 1

namespace NLOS {

    // stage 2: parse the raw data and bin it into a Fourier domain histogram.
    template<int NINDICES, int NFREQ>
    class FrameHistogramBuilder : public DataProcessor {
    public:
        FrameHistogramBuilder(ParsedSensorDataQueue& incoming, FrameHistogramDataQueue& outgoing, LogWriter& logWriter)
            : DataProcessor("FrameHistogramBuilder", logWriter)
            , m_incomingParsed(incoming)
            , m_outgoingHistograms(outgoing)
        {}

    protected:
        virtual void Initialize(const SceneParameters& sceneParameters);
        virtual void Work();
        virtual void OnStop();
    private:
        void BuildHistogram_OMP(
            const uint32_t* indexData,
            const float* timeData,
            const float* frequencyData,
            float* histData,
            const uint32_t nSamples);

        float m_frequencies[NFREQ];

#if USE_FDH_LOOKUP_TABLE
        int m_photon_minT;
        int m_photon_maxT;
        int m_numTimes;
        std::vector<float> m_sinLookup;
        std::vector<float> m_cosLookup;
        void BuildLookupTables();
#endif

        ParsedSensorDataQueue& m_incomingParsed;
        FrameHistogramDataQueue& m_outgoingHistograms;
    };
}
