#include "../util/spdlog/spdlog.h"
#include "FrameHistogramBuilder.h"
#include "../util/stopwatch.h"
#include <omp.h>

namespace NLOS {

    template<int NINDICES, int NFREQ>
    void FrameHistogramBuilder<NINDICES, NFREQ>::Initialize(const SceneParameters& sceneParameters)
    {
        if (sceneParameters.FreMask.size() != NFREQ) {
            throw std::logic_error(fmt::format("Incorrect number of frequencies. Try rebuilding exe with NUMBER_OF_FREQUENCIES = {} in compile_time_constants.h",
                sceneParameters.FreMask.size()));
        }
        if (sceneParameters.AptWidth * sceneParameters.AptHeight != NINDICES) {
            throw std::logic_error(fmt::format("Incorrect grid dimension. Try rebuilding exe with NUMBER_OF_ROWS={} and NUMBER_OF_COLS={} in compile_time_constants.h",
                sceneParameters.AptWidth, sceneParameters.AptHeight));
        }

        const float ds_multiplier = 1.f / (1 << sceneParameters.DownsamplingRate);
        const float ts = sceneParameters.Resolution * sceneParameters.Picosecond / ds_multiplier;
        const float photon_maxT = std::round(2 * sceneParameters.DepthMax / (ts * sceneParameters.C_Light));

        const float* fremask = sceneParameters.FreMask.data();
        for (int i = 0; i < NFREQ; i++) {
            // incorporate -2*pi/maxT into each frequency to to save 2 multiplications on each record.
            m_frequencies[i] = fremask[i] * (float)(-2. * M_PI / photon_maxT);
        }

#if USE_FDH_LOOKUP_TABLE
        m_photon_minT = (int)(sceneParameters.Z_Gate * ds_multiplier);
        m_photon_maxT = (int)photon_maxT;
        m_numTimes = m_photon_maxT - m_photon_minT + 1;
        BuildLookupTables();
#endif
    }

#if USE_FDH_LOOKUP_TABLE
    template<int NINDICES, int NFREQ>
    void FrameHistogramBuilder<NINDICES, NFREQ>::BuildLookupTables() {
        m_sinLookup.resize(NFREQ * m_numTimes);
        m_cosLookup.resize(NFREQ * m_numTimes);
        for (int t = 0; t < m_numTimes; t++) {
            for (int f = 0; f < NFREQ; f++) {
                m_sinLookup[t * NFREQ + f] = std::sin(m_frequencies[f] * (t + m_photon_minT));
                m_cosLookup[t * NFREQ + f] = std::cos(m_frequencies[f] * (t + m_photon_minT));
            }
        }
    }
#endif

    // This worker should receive T3Rec data from the the RawSensorDataQueue
    // It should parse the data and bin the data into a Fourier domain histogram.
    // The incoming data should be assumed to be in random sizes, and not a full frame at a time.
    // Once an entire frame is recieved and binned, the histogram for that frame
    // should be packaged and pushed to the outgoing queue.
    template<int NINDICES, int NFREQ>
    void FrameHistogramBuilder<NINDICES, NFREQ>::Work() {
        ParsedSensorDataPtr parsedData;
        while (!m_stopRequested) {
            // get the next frame of parsed photon data
            if (!m_incomingParsed.Pop(parsedData)) {
                spdlog::critical("{:<25}: failed to receive parsed photon data. Exiting.", m_name);
                break;
            }

            Stopwatch timer(true);
            // allocate the outgoing frame
            spdlog::trace("{:<25}: Received parsed data for frame {}, going to build FDH", m_name, parsedData->FrameNumber);
            FrameHistogramDataPtr fhd(new FrameHistogramDataType(parsedData->FrameNumber));

            // binning
            //spdlog::debug(",{:<25},{},{} photons to be binned for frame", m_name, parsedData->FrameNumber, parsedData->IndexData.size());
            BuildHistogram_OMP(parsedData->IndexData.data(), parsedData->TimeData.data(), m_frequencies, fhd->Histogram, (uint32_t)parsedData->IndexData.size());

            spdlog::debug(",{:<25},{},{:8.2f} ms to build histogram of {} photons for frame", m_name, parsedData->FrameNumber, timer.Stop(), parsedData->IndexData.size());

            // push histogram data into the outgoing queue
            spdlog::trace("{:<25}: Pushing frame {} to the outgoing queue (queue.size == {})", m_name, parsedData->FrameNumber, m_outgoingHistograms.Size());
            m_outgoingHistograms.Push(fhd);

            // if we are logging fdh data, push it into the queue to be logged.
            if (m_logWriter.LogFdhData()) {
                m_logWriter.PushLog(fhd);
            }
        }
        spdlog::trace("{:<25}: Exiting worker thread", m_name);
    }

    template<int NINDICES, int NFREQ>
    void FrameHistogramBuilder<NINDICES, NFREQ>::OnStop() {
        // there may be a push blocking if the queue is full, so tell the queue to abort it's operation
        spdlog::trace("{}: Abort", m_name);
        m_incomingParsed.Abort();
        m_outgoingHistograms.Abort();
    }

    template<int NINDICES, int NFREQ>
    void FrameHistogramBuilder<NINDICES, NFREQ>::BuildHistogram_OMP(
        const uint32_t* indexData,
        const float* timeData,
        const float* frequencyData,
        float* histData,
        const uint32_t nSamples)
    {
        // optimal ordering for cuda rsd input
        using HistogramArrayType = float(&)[NFREQ][NINDICES][2];
        HistogramArrayType histogram = reinterpret_cast<HistogramArrayType>(*histData);
        memset(histData, 0x00, NFREQ * NINDICES * 2 * sizeof(float));

        // note, the -2PI/MaxT coefficient is already multiplied into the frequencyData
#pragma omp parallel for num_threads(36)
        for (int f = 0; f < NFREQ; f++) {
            for (uint32_t s = 0; s < nSamples; s++) {
#if USE_FDH_LOOKUP_TABLE
                int idx = (int)((timeData[s] + 0.5f) - m_photon_minT) * NFREQ + f;
                histogram[f][indexData[s]][0] += m_cosLookup[idx]; //std::cos(frequencyData[f] * timeData[s]);
                histogram[f][indexData[s]][1] += m_sinLookup[idx]; //std::sin(frequencyData[f] * timeData[s]);
#else
                histogram[f][indexData[s]][0] += std::cos(frequencyData[f] * timeData[s]);
                histogram[f][indexData[s]][1] += std::sin(frequencyData[f] * timeData[s]);
#endif
            }
        }
    }

    // explicit instantiation
    template class FrameHistogramBuilder<NUMBER_OF_ROWS * NUMBER_OF_COLS, NUMBER_OF_FREQUENCIES>;

}
