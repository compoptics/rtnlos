#pragma once

#include <memory>
#include "../util/SafeQueue.h"
#include "T3Rec.h"
#include "PipelineData.h"

namespace NLOS
{
	template<int NUM_RECORDS>
	class RawSensorData : public PipelineData {
	public:
		RawSensorData() 
			: NumRecords(0)
			, FileReaderWasResetFlag(false)
		{ }

		int NumRecords;
		T3Rec Records[NUM_RECORDS];
		bool FileReaderWasResetFlag;

		virtual void LogToFile(ILogContext* pContext) {
			const long MAX_LOG_FILE_SIZE = (1 << 30); // 1gb 
			try {
				if (pContext->RawLogStream().tellp() < MAX_LOG_FILE_SIZE) // for your own safety, don't fill the entire disk.
					pContext->RawLogStream().write((const char*)Records, NumRecords * sizeof(T3Rec));
				else
					spdlog::warn("Raw log file is already > {} mb. Skipping logging", MAX_LOG_FILE_SIZE >> 20);
			}
			catch (const std::ostream::failure& ex) {
				spdlog::warn("Failed to write raw records to log file. Error was: {}", ex.what());
			}
		};
	};
}