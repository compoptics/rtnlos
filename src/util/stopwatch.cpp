/*
 * Stopwatch.cpp
 *
 *  Created on: Jul 11, 2019
 *      Author: ebrandt
 */

#include <iostream>
#include <iomanip>
#include <chrono>

#include "stopwatch.h"

using namespace std;
using namespace std::chrono;

Stopwatch::Stopwatch(bool autostart) : m_count(0), m_sum(0.0) {
	if (autostart) {
		Start();
	}
}

void Stopwatch::Start()
{
	m_startTime = high_resolution_clock::now();
}

double Stopwatch::Stop()
{
	auto t = high_resolution_clock::now();
	auto duration = duration_cast<microseconds>(t - m_startTime);
	double ms = (double)(duration.count() / 1000.0);
	m_times.push_back(ms);
	m_sum += ms;
	m_count++;
    return ms;
}

double Stopwatch::Average() const
{
	return (m_count == 0) ? 0.0 : m_sum / m_count;
}

void Stopwatch::Reset()
{
	m_count = 0;
	m_sum = 0.0;
	m_times.clear();
}

/////////////////////////////
FunctionTimer::~FunctionTimer()
{
	double tm = Stop();
	cout << m_name << ": " << std::fixed << std::setprecision(6) << tm << endl;
}
