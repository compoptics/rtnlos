#pragma once

#include <thread>
#include <atomic>
#include <thread>
#include "spdlog/spdlog.h"
#include "cxxopts/cxxopts.hpp"
#include "../data/SceneParameters.h"

namespace NLOS {
    class LogWriter;

    class DataProcessor {
    public:
        DataProcessor(const std::string& name, LogWriter& logWriter)
            : m_name(name)
            , m_stopRequested(false)
            , m_logWriter(logWriter)
        {}

        virtual void InitCmdLineOptions(cxxopts::Options& options) { }
        virtual void Initialize(const SceneParameters& sceneParameters) { }

        void Start() {
            if (!m_thread.joinable()) {
                spdlog::trace("{}: Starting", m_name);
                std::thread worker(&DataProcessor::DoWork, this);
                m_thread = std::move(worker);
            }
        }

        void Stop() {
            spdlog::trace("{}: Stop requested", m_name);
            m_stopRequested = true;
            OnStop();
            m_thread.join();
            spdlog::info("{}: Stopped", m_name);
        }

    protected:
        virtual void Work() = 0;
        virtual void OnStop() = 0;

        const std::string m_name;
        std::atomic<bool> m_stopRequested;
        LogWriter& m_logWriter;
        std::thread m_thread;
    private:
        void DoWork() {
            spdlog::trace("{}: Started", m_name);
            try {
                Work();
            }
            catch (const std::exception & ex) {
                spdlog::critical("{}: Exception: {}", m_name, ex.what());
            }
            spdlog::trace("{}: Finished", m_name);
        }
    };
}
