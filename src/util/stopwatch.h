/*
 * Stopwatch.h
 *
 *  Created on: Jul 11, 2019
 *      Author: ebrandt
 */

#ifndef STOPWATCH_H_
#define STOPWATCH_H_

#include <chrono>
#include <string>
#include <vector>

using namespace std;
using namespace std::chrono;

// Simple stopwatch class
class Stopwatch {
public:
	Stopwatch(bool autostart = false);

	void Start();
	double Stop();
	void Reset();
	int Count() const { return m_count; }
	double Average() const;
	double Total() const { return m_sum; }
	const vector<double>& History() { return m_times; }

private:
    high_resolution_clock::time_point m_startTime;
	
	vector<double> m_times;
	double m_sum;
	int m_count;
};

// simple function timer using RAII
class FunctionTimer : public Stopwatch {
public:
	FunctionTimer(string name) : Stopwatch(true), m_name(name) { };
	virtual ~FunctionTimer();

private:
	string m_name;
};

#endif /* STOPWATCH_H_ */
