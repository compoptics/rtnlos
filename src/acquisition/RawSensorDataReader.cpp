#include "RawSensorDataReader.h"
#include "../util/spdlog/spdlog.h"
#include "../compile_time_constants.h"
#include "../util/stopwatch.h"

extern "C" {
#include "../util/hhlib/hhdefin.h"
#include "../util/hhlib/hhlib.h"
#include "../util/hhlib/errorcodes.h"
};



namespace NLOS {

	void RawSensorDataReader::InitCmdLineOptions(cxxopts::Options& options)
	{
		// add any command line options specific to the data reader here.
		options.add_options(m_name)
			("f,file", "read raw data from file", cxxopts::value<std::string>(m_rawDataFileName));
	}

	// This worker should read T3Rec data from the hardware (or data file)
	// It should not do any parsing of the data.
	// It should package it into an array of whatever size it decides
	// is appropriate (best size can be fine tuned by testing)
	// The resulting array of records should be pushed to the outgoing queue.
	void RawSensorDataReader::Work()
	{
		if (m_rawDataFileName.size() > 0)
			ReadFromFile();
        else {
#if _WIN64
            ReadFromDevice();
#else // !_WIN64
            spdlog::critical("{:<25}: Device acquisition only available on Windows OS", m_name);
#endif 
        }
	}

    void RawSensorDataReader::OnStop() {
        // there may be a push blocking if the queue is full, so tell the queue to abort its operation
        spdlog::trace("{:<25}: Abort", m_name);
        m_outgoingRaw.Abort();
    }


	// this function is used to read the input data from a raw log file 
	// for offline testing purposes.
	void RawSensorDataReader::ReadFromFile()
	{
		// open the file
		FILE* fp = fopen(m_rawDataFileName.c_str(), "rb");
		if (fp == NULL) {
			throw std::logic_error("cannot open file");
		}

		// how many records does it contain?
		fseek(fp, 0L, SEEK_END);
		int totalRecords = ftell(fp) / sizeof(T3Rec);
		if (ftell(fp) % sizeof(T3Rec) != 0 || totalRecords == 0)
			throw std::logic_error("file size is not a multiple of record size");
		rewind(fp);

		// read all the records into an array
		std::unique_ptr<T3Rec> recs(new T3Rec[totalRecords]);
		if (recs == nullptr)
			throw std::logic_error("allocation error");
		size_t ret = fread(recs.get(), sizeof(T3Rec), totalRecords, fp);
		if (ret != totalRecords) {
			throw std::logic_error("unable to read full file");
		}

		// being looping through the file's records forever
		int curRecord = 0;
		while (!m_stopRequested) {
			Stopwatch timer(true);
		
			RawSensorDataPtr chunk(new RawSensorDataType());

			if (curRecord >= totalRecords) { // if we're at the end, reset to beginning of file.
				curRecord = 0;
			}

			if (curRecord == 0) { // if we're at the beginning, notify the next stage that we reset
				spdlog::trace("{:<25}: File reader resetting to beginning", m_name);
				chunk->FileReaderWasResetFlag = true;
			}

			chunk->NumRecords = std::min(RAW_SENSOR_READ_BLOCK_SIZE, totalRecords - curRecord);
			memcpy(chunk->Records, &(recs.get()[curRecord]), chunk->NumRecords * sizeof(T3Rec));
			curRecord += chunk->NumRecords;

			//spdlog::trace("{:<25}: filled outgoing buffer with {} records in {} msec. Pushing to queue (sz={})", m_name, chunk->NumRecords, timer.Stop(), m_outgoingRaw.Size());

			// push onto outgoing queue
			m_outgoingRaw.Push(chunk);

            // if we are logging raw data, push it into the queue to be logged.
            if (m_logWriter.LogRawData()) {
                m_logWriter.PushLog(chunk);
            }

			if (m_stopRequested) {
				break;
			}

			// wait a bit (for demo purposes only)
			//std::this_thread::sleep_for(std::chrono::milliseconds(50));
		}
	}

#if _WIN64
	void RawSensorDataReader::ReadFromDevice() {

        //make this a high priority thread.
        BOOL ret = SetThreadPriority(m_thread.native_handle(), THREAD_PRIORITY_ABOVE_NORMAL);  // base_priority+1
        if (ret)
            spdlog::debug("{:<25}: Set thread priority to THREAD_PRIORITY_ABOVE_NORMAL", m_name);
        else
            spdlog::critical("{:<25}: FAILED to set thread priority to THREAD_PRIORITY_ABOVE_NORMAL. err={}", m_name, GetLastError());

		unsigned int Progress = 0;
        int device_id = HardwareStart();
		
		while (!m_stopRequested)
		{
			//Sleep(1);
			RawSensorDataPtr recs(new RawSensorDataType());
			
            int flags;
			int retcode = HH_GetFlags(device_id, &flags);
			if (retcode < 0)
			{
				printf("\nHH_GetFlags error %1d. Aborted.\n", flags);
				break;
			}

			if (flags & FLAG_FIFOFULL)
			{
				printf("\nFiFo Overrun!\n");
				break;
			}

			retcode = HH_ReadFiFo(device_id, (unsigned int*)recs->Records, TTREADMAX, &recs->NumRecords);	//may return less!
			if (retcode < 0)
			{
				printf("\nHH_ReadFiFo error %d. Aborted.\n", retcode);
				break;
			}

			if (recs->NumRecords)
			{
				// push onto outgoing queue
				//spdlog::debug("{:<25}: Pushing {} records to the outgoing queue (queue.size == {})", m_name, recs->NumRecords, m_outgoingRaw.Size());
				m_outgoingRaw.Push(recs);
				Progress += recs->NumRecords;
				//printf("\b\b\b\b\b\b\b\b\b%9u", Progress);

                // if we are logging raw data, push it into the queue to be logged.
                if (m_logWriter.LogRawData()) {
                    m_logWriter.PushLog(recs);
                }
			}
			else
			{
				int ctcstatus;
				retcode = HH_CTCStatus(device_id, &ctcstatus);
				if (retcode < 0)
				{
					printf("\nHH_StartMeas error %d. Aborted.\n", retcode);
					break;
				}
				if (ctcstatus)
				{
					printf("\nDone\n");
					break;
				}
			}
		}
		HardwareStop(device_id);
	}

    int RawSensorDataReader::HardwareStart()
    {
        int found = 0;
        int retcode;
        char LIB_Version[8];
        char HW_Model[16];
        char HW_Partno[8];
        char HW_Version[8];
        char HW_Serial[8];
        char Errorstring[40];
        int NumChannels;
        int Mode = MODE_T3; //set T2 or T3 here, observe suitable Syncdivider and Range!
        int Binning = 3; //you can change this, meaningful only in T3 mode
        int Offset = 0;  //you can change this, meaningful only in T3 mode
        int Tacq = 1000000; //Measurement time in millisec, you can change this
        int SyncDivider = 1; //you can change this, observe Mode! READ MANUAL!
        int SyncCFDZeroCross = 10; //you can change this (in mV)
        int SyncCFDLevel = 50; //you can change this (in mV)
        int SyncChannelOffset = 89700; //you can change this (in ps, like a cable delay)
        int InputCFDZeroCross = 5; //you can change this (in mV)
        int InputCFDLevel = 50; //you can change this (in mV)
        int InputChannelOffset = -20000; //you can change this (in ps, like a cable delay)
        double Resolution;
        int Syncrate;
        int Countrate;
        int i;


        HH_GetLibraryVersion(LIB_Version);
        spdlog::info("{:<25}: Library version is {}", m_name, LIB_Version);
        if (strncmp(LIB_Version, LIB_VERSION, sizeof(LIB_VERSION)) != 0)
            spdlog::warn("{:<25}: Warning: The application was built for version %s.", m_name, LIB_VERSION);

        spdlog::info("{:<25}: Mode               : {}\n", "HHApi", Mode);
        spdlog::info("{:<25}: Binning            : {}\n", "HHApi", Binning);
        spdlog::info("{:<25}: Offset             : {}\n", "HHApi", Offset);
        spdlog::info("{:<25}: AcquisitionTime    : {}\n", "HHApi", Tacq);
        spdlog::info("{:<25}: SyncDivider        : {}\n", "HHApi", SyncDivider);
        spdlog::info("{:<25}: SyncCFDZeroCross   : {}\n", "HHApi", SyncCFDZeroCross);
        spdlog::info("{:<25}: SyncCFDLevel       : {}\n", "HHApi", SyncCFDLevel);
        spdlog::info("{:<25}: SyncChannelOffset  : {}\n", "HHApi", SyncChannelOffset);
        spdlog::info("{:<25}: InputCFDZeroCross  : {}\n", "HHApi", InputCFDZeroCross);
        spdlog::info("{:<25}: InputCFDLevel      : {}\n", "HHApi", InputCFDLevel);
        spdlog::info("{:<25}: InputChannelOffset : {}\n", "HHApi", InputChannelOffset);

        spdlog::info("{:<25}: Searching for HydraHarp devices...", m_name);
        spdlog::info("{:<25}: Devidx     Status", "HHApi");

        int devices[MAXDEVNUM];
        for (i = 0; i < MAXDEVNUM; i++)
        {
            retcode = HH_OpenDevice(i, HW_Serial);
            if (retcode == 0) //Grab any HydraHarp we can open
            {
                spdlog::info("{:<25}:   {}        S/N {}", "HHApi", i, HW_Serial);
                devices[found] = i; //keep index to devices we want to use
                found++;
            }
            else
            {
                if (retcode == HH_ERROR_DEVICE_OPEN_FAIL)
                    spdlog::info("{:<25}:   {}        no device", "HHApi", i);
                else
                {
                    HH_GetErrorString(Errorstring, retcode);
                    spdlog::error("{:<25}:   {}        S/N {}", "HHApi", i, Errorstring);
                }
            }
        }

        if (found < 1)
        {
            spdlog::critical("{:<25}: No device available.", "HHApi");
            throw std::logic_error("error");
        }
        spdlog::info("{:<25}: Using device #{}", "HHApi", devices[0]);
        spdlog::info("{:<25}: Initializing the device...", "HHApi");

        fflush(stdout);

        retcode = HH_Initialize(devices[0], Mode, 0);  //with internal clock
        if (retcode < 0)
        {
            spdlog::critical("{:<25}: HH_Initialize error {}. Aborted.", "HHApi", retcode);
            throw std::logic_error("error");
        }

        retcode = HH_GetHardwareInfo(devices[0], HW_Model, HW_Partno, HW_Version); //this is is only for information
        if (retcode < 0)
        {
            spdlog::critical("{:<25}: HH_GetHardwareInfo error {}. Aborted.", "HHApi", retcode);
            throw std::logic_error("error");
        }
        else
            spdlog::info("{:<25}: Found Model {} Part no {} Version {}", "HHApi", HW_Model, HW_Partno, HW_Version);

        retcode = HH_GetNumOfInputChannels(devices[0], &NumChannels);
        if (retcode < 0)
        {
            spdlog::critical("{:<25}: HH_GetNumOfInputChannels error {}. Aborted.", "HHApi", retcode);
            throw std::logic_error("error");
        }
        else
            spdlog::info("{:<25}: Device has {} input channels.", "HHApi", NumChannels);

        fflush(stdout);

        printf("\nCalibrating...");
        retcode = HH_Calibrate(devices[0]);
        if (retcode < 0)
        {
            spdlog::critical("{:<25}: Calibration Error {}. Aborted.", "HHApi", retcode);
            throw std::logic_error("error");
        }

        retcode = HH_SetSyncDiv(devices[0], SyncDivider);
        if (retcode < 0)
        {
            spdlog::critical("{:<25}: PH_SetSyncDiv error {}. Aborted.", "HHApi", retcode);
            throw std::logic_error("error");
        }

        retcode = HH_SetSyncCFD(devices[0], SyncCFDLevel, SyncCFDZeroCross);
        if (retcode < 0) {
            spdlog::critical("{:<25}: HH_SetSyncCFD error {}. Aborted.", "HHApi", retcode);
            throw std::logic_error("error");
        }

        retcode = HH_SetSyncChannelOffset(devices[0], SyncChannelOffset);
        if (retcode < 0) {
            spdlog::critical("{:<25}: HH_SetSyncChannelOffset error {}. Aborted.", "HHApi", retcode);
            throw std::logic_error("error");
        }

        for (i = 0; i < NumChannels; i++) { // we use the same input settings for all channels
            retcode = HH_SetInputCFD(devices[0], i, InputCFDLevel, InputCFDZeroCross);
            if (retcode < 0) {
                spdlog::critical("{:<25}: HH_SetInputCFD error {}. Aborted.", "HHApi", retcode);
                throw std::logic_error("error");
            }

            retcode = HH_SetInputChannelOffset(devices[0], i, InputChannelOffset);
            if (retcode < 0) {
                spdlog::critical("{:<25}: HH_SetInputChannelOffset error {}. Aborted.", "HHApi", retcode);
                throw std::logic_error("error");
            }
        }

        if (Mode == MODE_T3) {
            retcode = HH_SetBinning(devices[0], Binning); //Meaningful only in T3 mode
            if (retcode < 0) {
                spdlog::critical("{:<25}: HH_SetBinning error {}. Aborted.", "HHApi", retcode);
                throw std::logic_error("error");
            }

            retcode = HH_SetOffset(devices[0], Offset); //Meaningful only in T3 mode
            if (retcode < 0) {
                spdlog::critical("{:<25}: HH_SetOffset error {}. Aborted.", "HHApi", retcode);
                throw std::logic_error("error");
            }

            retcode = HH_GetResolution(devices[0], &Resolution); //Meaningful only in T3 mode
            if (retcode < 0) {
                spdlog::critical("{:<25}: HH_GetResolution error {}. Aborted.", "HHApi", retcode);
                throw std::logic_error("error");
            }

             spdlog::info("{:<25}: Resolution is {:1.1f}ps.", "HHApi", Resolution);
        }

        //Note: after Init or SetSyncDiv you must allow >100 ms for valid new count rate readings
        Sleep(200);

        retcode = HH_GetSyncRate(devices[0], &Syncrate);
        if (retcode < 0) {
            spdlog::critical("{:<25}: HH_GetSyncRate error {}. Aborted.", "HHApi", retcode);
            throw std::logic_error("error");
        }
        spdlog::info("{:<25}: Syncrate={}/s", "HHApi", Syncrate);

        for (i = 0; i < NumChannels; i++) { // for all channels
            retcode = HH_GetCountRate(devices[0], i, &Countrate);
            if (retcode < 0) {
                spdlog::critical("{:<25}: HH_GetCountRate error {}. Aborted.", "HHApi", retcode);
                throw std::logic_error("error");
            }
            spdlog::info("{:<25}: Countrate[{}]={}/s", "HHApi", i, Countrate);
        }

        retcode = HH_StartMeas(devices[0], Tacq);
        if (retcode < 0) {
            spdlog::critical("{:<25}: HH_StartMeas error {}. Aborted.", "HHApi", retcode);
            throw std::logic_error("error");
        }

        return devices[0];
    }

    void RawSensorDataReader::HardwareStop(int device_id)
    {
        int retcode = HH_StopMeas(device_id);
        if (retcode < 0) {
            spdlog::critical("{:<25}: HH_StopMeas error {}. Aborted.", "HHApi", retcode);
            throw std::logic_error("error");
        }

        for (int i = 0; i < MAXDEVNUM; i++) { //no harm to close all
            HH_CloseDevice(i);
        }
    }
#endif // _WIN64
}
