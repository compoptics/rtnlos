#include <iostream>
#include <memory>
#include "spdlog/spdlog.h"
#include "cxxopts/cxxopts.hpp"
#include "types.h"
#include "NLOSStreamingEngine.h"
#include "compile_time_constants.h"

using namespace NLOS;

int main(int argc, char *argv[])
{
	using EngineType = NLOSStreamingEngine<NUMBER_OF_ROWS, NUMBER_OF_COLS, NUMBER_OF_FREQUENCIES>;

	bool debug = false, help = false;
	std::string rawDataFile;

	try {
		spdlog::info("Streaming NLOS Image Reconstruction");
		spdlog::info("University of Wisconsin, Madison, 2020");
		spdlog::info("Press enter key to stop...");
		spdlog::info("");

		EngineType engine(argc, argv);

		engine.Start();
	}
	catch(const std::exception & ex) {
		spdlog::critical("Fatal error: {}", ex.what());
		exit(-1);
	}
	
	exit(0);
}
