#pragma once

// compile time constants (used to populate template class specializations)

#define NUMBER_OF_ROWS 190
#define NUMBER_OF_COLS 190
#define NUMBER_OF_FREQUENCIES 208
#define RAW_SENSOR_READ_BLOCK_SIZE (131072) // copied from TTREADMAX
